import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Mascota } from '../../models/mascota-interface';
import { MascotasService } from '../../services/mascotas.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-mascota',
  templateUrl: './add-mascota.component.html',
  styleUrls: ['./add-mascota.component.css']
})
export class AddMascotaComponent implements OnInit {

  mascota:Mascota = {
    edad: '',
  	nombre: '',
    raza: ''
  }

  status: boolean = false; 
  id: string;
  estadoPositivo: boolean = false;

  constructor(private servicio: MascotasService,
              private router: Router,
              private route: ActivatedRoute){ 

          this.route.params.subscribe( parametros => {
                console.log("Id desde activated route: ",parametros);
                this.id = parametros['id'];

              if(this.id !== 'nuevo'){ 
                this.servicio.obtenerMascota(this.id).subscribe((mascota) => {
                    this.mascota = mascota.payload.data() as Mascota;
                    this.estadoPositivo = true;
                    console.log(this.mascota);
                });
              }

          });

  }

  ngOnInit() {
  }

  guardar(){

  	console.log(this.mascota);

    if(this.id == 'nuevo'){
        this.servicio.nuevaMascota(this.mascota).then((data) => {
           console.log("Mascota agregada con la ID", data.id);
           this.router.navigate(['/mascota', data.id]);
        })
        .catch((error) => {
          console.log("Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", error.message)
        });
    }
    else{
      	this.servicio.actualizarMascota(this.mascota, this.id).then((data) => {
           this.router.navigate(['/mascotas']);
        })
        .catch((error) => {
          console.log("Errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", error.message)
        });
    }

  }

  resetear(forma: NgForm){
     this.router.navigate(['/mascota', 'nuevo']);
     forma.reset();
     this.estadoPositivo = false;
  }

}
