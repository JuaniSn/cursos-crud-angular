import { Component, EventEmitter, Output} from '@angular/core';
import { ComidasService } from '../../services/comidas.service';

@Component({
  selector: 'barrita',
  template: `<nav class="#263238 blue-grey darken-4">
    <div class="nav-wrapper">
      <form>
        <div class="input-field">
          <input [(ngModel)]="search" (keypress)="buscarComida()"
           id="search" name="search" type="search" placeholder="Buscar" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>`,
  styles: []
})

export class BuscarComponent {

  @Output() res: EventEmitter<any[]> = new EventEmitter()

  comida:any[];
  search: string = "";

  constructor(private _comida: ComidasService) { }

  buscarComida(){
  	if(this.search.length == 0){
         this._comida.getComidas().subscribe(data => {
             this.comida = data;
             this.res.emit(this.comida);
         });
  	}
  	this.comida = this._comida.obtenerComida(this.search);
     this.res.emit(this.comida)

  }
}
