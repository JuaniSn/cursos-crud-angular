import { Component, OnInit } from '@angular/core';
import { ComidasService } from '../../services/comidas.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var M: any;

@Component({
  selector: 'app-lista-comida',
  templateUrl: './lista-comida.component.html',
  styleUrls: ['./lista-comida.component.css']
})
export class ListaComidaComponent implements OnInit {
  
  comidas: any;
  p: number = 1;
  
  constructor(private _comidas: ComidasService, private router: Router,
                    private route: ActivatedRoute){ 
  	this._comidas.getComidas().subscribe(data => {
  		this.comidas = data;
  	});
  }

  ngOnInit() {
  	var elems = document.querySelectorAll('.action-btn');
    var instances = M.FloatingActionButton.init(elems, { direction: 'left', hoverEnabled: false});
  }

  eliminarComida(id: string){
    this._comidas.dropComida(id).then(data => {
      console.log(data);
       M.toast({html: '<span style="color: red">Eliminado</span>', classes: '#fafafa grey lighten-5 bordes rounded'});
    })
  }

  editarComida(id: string){
    this.router.navigate(['/comidas/editar', id]);
  }



}
