import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  mensaje: string = '';
  element: any;

  constructor(public chat: ChatService) { 
  	this.chat.cargarMensajes().subscribe( () => {
      setTimeout( () => {
          this.element.scrollTop = this.element.scrollHeight;
      }, 20); 
    });
  }

  ngOnInit() {
    this.element = document.getElementById('app-mensajes');
  }


  enviar_mensaje(){
  	console.log(this.mensaje);

    if(this.mensaje == '0'){
      return;
    }

    this.chat.agregarMensaje(this.mensaje).then( () => this.mensaje = '').catch( (err) => console.log("Error al enviar"));
  }

}
