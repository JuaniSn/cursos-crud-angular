import { Component, OnInit } from '@angular/core';
import { CursoInterface } from '../../models/curso-interface';
import { CursoService } from '../../services/curso.service';

declare var M:any;

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  
  cursos: CursoInterface[];
  editState: boolean = false;
  cursoToEdit: CursoInterface; 
  cargando: boolean = true;
  languageOption:string[];
  lenguajeDefault:string;

  constructor(private cursoService: CursoService) {
    
    this.languageOption = ['English', 'Spanish'];
    this.cursoService.getCursos().subscribe(cursos => {
           // setTimeout( () => {
              this.cargando = false;
              this.cursos = cursos;
              console.log(this.cursos)
           // },3000);
    });
  }

  ngOnInit(){}

  ngAfterViewInit() {
      var elems = document.querySelectorAll('select');
      var instances = M.FormSelect.init(elems);
      console.log(elems)
  }

  clearState(){
    this.editState = false;
    this.cursoToEdit = null;
  }

  editarCurso(event, curso: any){
    this.editState = true;
    this.cursoToEdit = curso;
    this.lenguajeDefault = curso.data.language;
  }

  updateCurso(curso:CursoInterface){
     this.cursoService.actualizarCurso(curso);
     this.clearState();
  }

  borrarCurso(event, curso:CursoInterface){
     this.cursoService.borrarCurso(curso);
     this.clearState();
  }

}
