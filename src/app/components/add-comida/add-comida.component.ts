import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { Router, ActivatedRoute } from '@angular/router';

import { ComidaInterface } from '../../models/comida-interface';

import { FileItem } from '../../class/file-item';

import { ComidasService } from '../../services/comidas.service';
import { CargaImagenService } from '../../services/carga-imagen.service';

import { FormBuilder, FormGroup, Validators} from '@angular/forms';

declare var M:any;

@Component({
  selector: 'app-add-comida',
  templateUrl: './add-comida.component.html',
  styleUrls: ['./add-comida.component.css']
})
export class AddComidaComponent implements OnInit {
  
   myForm: FormGroup;

  comida: ComidaInterface = {
    titulo: '',
    descripcion: '', 
    ingredientes: '',
    preparacion: '',
    persona: '',
    fecha: 0,
    url: ''
  }

  imagenPuesta: boolean = false;
  archivo: FileItem[] = [];
  imagenEnDrop = false;
  id: string;
  editar: boolean = false;

  constructor(public _comida: ComidasService, 
                    private storage: AngularFireStorage, 
                    public fb: FormBuilder,  
                    private router: Router,
                    private rutaActiva: ActivatedRoute){

    this.myForm = this.fb.group({
      titulo: ['', [Validators.required, Validators.maxLength(20)]],
      ingredientes: ['',[Validators.required]],
      descripcion: ['',[Validators.required]],
      preparacion: ['',[Validators.required]]
    });


    this.rutaActiva.params.subscribe( URL => {
        this.id = URL.id;

        if(this.id != undefined){
          this._comida.getComidaEditar(this.id).subscribe( data => {
            this.comida = data.payload.data() as ComidaInterface;
            this.editar = true;
          })
        }
    })

 }

  ngOnInit() {
  }


  guardarComida(){

     this.comida.fecha = Date.now();
     this.comida.persona = 'Juan Reyes';

     if(this.archivo.length > 0){
       console.log("si hay")
       const archivo = this.archivo[0].archivo;
       this.subirImagen(archivo);
     }
     else{
       M.toast({html: '<span style="color: red">Debe montar una imagen</span>', classes: '#fafafa grey lighten-5 bordes rounded'});
       console.log("No hay")
     }
  }

  subirImagen(archivo: File){
     const filePath = '/comidas/pic-' + Math.floor(Math.random() * 1000000);
     const task = this.storage.upload(filePath, archivo);
      task.percentageChanges().subscribe( value => {
        this.archivo[0].progreso = value;
      });

     task.then(() => {
           const ref = this.storage.ref(filePath);

           const downloadURL = ref.getDownloadURL().subscribe(url => { 
               this.comida.url = url;
               this._comida.subirComida(this.comida).then(() => {
                   M.toast({html: '<span style="color: green">Registrado con éxito</span>', classes: '#fafafa grey lighten-5 bordes rounded'});
                   this.router.navigate(['/comidas/lista']);
               });
           });
    });  
  }


  montarImagen(event){
    event.preventDefault();
    event.stopPropagation();
    const nuevoArchivo = new FileItem(event.target.files[0]);
    this.archivo.push(nuevoArchivo);         
    this.imagenPuesta = true;
  }

  limpiar(){
    this.archivo = [];
    this.imagenPuesta = false;
  }

}
