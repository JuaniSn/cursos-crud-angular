import { Component } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Http } from '@angular/http';
declare var M:any;

@Component({
  selector: 'app-prueba-directiva',
  templateUrl: './prueba-directiva.component.html',
  styleUrls: ['./prueba-directiva.component.css']
})
export class PruebaDirectivaComponent {

  selectedFiles: FileList;
  file: File;
  imgsrc;
  carga;
  myForm: FormGroup;

  objeto: any = {

  edad: '',
  peso: '',
  talla: '',
  genero: '',
  embarazo: '',
  meses: '',
  cbi: '',
  complicacion_clinica: ''
  }

  cargando: boolean = false;
  constructor(private storage: AngularFireStorage, public fb: FormBuilder, private http: Http){ 
  	 this.myForm = this.fb.group({
      name: ['', [Validators.required]],
      company: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(10)]],
      email: ['', [Validators.required]],
      age: ['', [Validators.required]],
      url: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }


saveData(){
    console.log(this.myForm.value);
  }

  situacion(){	
  	this.cargando = true;
     this.http.get('http://localhost/excel/index.php/Welcome/pruebaAngula2r/'+this.objeto.edad).subscribe(data => {
     	this.cargando = false;
     	let situacion = data.json();
     	   M.toast({html: '<span style="color: red">'+situacion+'</span>', classes: '#fafafa grey lighten-5 bordes rounded'});

     })
  }


	chooseFiles(event){
		this.selectedFiles = event.target.files;
		if(this.selectedFiles.item(0))
			this.uploadImagen();
	}

	uploadImagen(){
		const file = this.selectedFiles.item(0);
		const filePath = '/galeria/pic' + Math.floor(Math.random() * 1000000);
	    const task = this.storage.upload(filePath, file);
	    task.percentageChanges().subscribe( value => {
	    	this.carga = value;
	    });
	    console.log("PAsa de largo", this.carga);
	    task.then(() => {
	         const ref = this.storage.ref(filePath);
	         const downloadURL = ref.getDownloadURL().subscribe(url => { 
	         this.imgsrc = url 
	         console.log(url);
	     	});
		});

	    console.log("Llega incluso aqui")
	}

}
