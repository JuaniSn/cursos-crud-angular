import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaDirectivaComponent } from './prueba-directiva.component';

describe('PruebaDirectivaComponent', () => {
  let component: PruebaDirectivaComponent;
  let fixture: ComponentFixture<PruebaDirectivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaDirectivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaDirectivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
