import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';
import { GalleryComponent } from './gallery.component';
import { GalleryService } from '../../services/gallery.service';
import { GalleryInterface } from '../../models/gallery-interface';


@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.css']
})
export class AddPhotoComponent implements OnInit {

  galeria: GalleryInterface[];

  constructor(private http: Http, private gallery: GalleryComponent, private service: GalleryService) { }

  ngOnInit() {
  }

  addImage(event){
  	console.log(event);
  	let elemento = event.target;

  	if(elemento.files.length > 0){
  		let formData = new FormData();
  		formData.append('file', elemento.files[0]);
      this.http.post('http://localhost/angular/backend/script.php', formData).subscribe((data) => {
        setTimeout(() => {
          let respuesta = data.json();
          this.gallery.dataBackend(respuesta);  
         }, 3000);
      },(error) => console.log(error.message));
  	}
  }

}
