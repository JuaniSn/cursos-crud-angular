import { Component, OnInit } from '@angular/core';
import { GalleryService } from '../../services/gallery.service';

declare var M:any;

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  id: string = '';
  images: any;

  constructor(private service: GalleryService) { }

  ngOnInit() {
  	this.service.a().subscribe((images) => {
      this.images = []
      images.forEach((images: any)=> {
        this.images.push({
          id: images.payload.doc.id,
          data: images.payload.doc.data()
        });
      })
      // this.galeria = a[0].data;
       console.log(this.images);
    });

     document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.materialboxed');
      var instances = M.Materialbox.init(elems);
    });
  }

  dataBackend(uid){
  	this.service.agregarImagen(uid);
  }

}
