import { Component, OnInit } from '@angular/core';
import { MascotasService } from '../../services/mascotas.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var M:any;

@Component({
  selector: 'app-mascotas',
  templateUrl: './mascotas.component.html',
  styleUrls: ['./mascotas.component.css']
})
export class MascotasComponent implements OnInit {

  mascotas:any;
  cargando:boolean = true;

  constructor(private servicio: MascotasService,
  			      private router: Router,
              private route: ActivatedRoute){
  	this.servicio.obtenerMascotas().subscribe(data => {
  		    // setTimeout( () => {
              this.cargando = false
              this.mascotas = data;
           // },3000);
  	});

  }

  ngOnInit() {

  }

  editarMascota(mascotaId){
  	this.router.navigate(['/mascota', mascotaId]);
  }
  eliminarMascota(mascotaId){
  	this.servicio.eliminarMascota(mascotaId).then(data => {console.log("eliminado ", data)});
  }

}
