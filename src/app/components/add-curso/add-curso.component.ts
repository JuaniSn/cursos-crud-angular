import { Component, OnInit } from '@angular/core';
import { CursoService } from '../../services/curso.service';
import { CursoInterface } from '../../models/curso-interface';
import { NgForm } from '@angular/forms/src/directives/ng_form';

declare var M:any;

@Component({
  selector: 'app-add-curso',
  templateUrl: './add-curso.component.html',
  styleUrls: ['./add-curso.component.css']
})
export class AddCursoComponent implements OnInit {
  
  curso: CursoInterface = {
  	name: '',
  	facilitator: '',
  	price: '',
  	language: '',
  	technology: '',
  	date: '',
  	description: ''
  };

  languageOption:string[];

  constructor(private elService: CursoService) {
  	this.languageOption = ['English', 'Spanish'];
  }

  ngAfterViewInit() {
      var elems = document.querySelectorAll('select');
      var instances = M.FormSelect.init(elems);
  }

  ngOnInit(){
    
  }

  guardarCurso(form: NgForm){
  	const fecha_actual = Date.now();
  	this.curso.date = fecha_actual;
  	this.elService.agregarCurso(this.curso);
    this.curso = {};
  }

  verObjetos(event){
  	console.log(event);
  }

}
