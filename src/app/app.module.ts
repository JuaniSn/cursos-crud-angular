import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from 'angularfire2/firestore'
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { NgxPaginationModule } from 'ngx-pagination';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_ROUTING } from './app.routes';
import { HttpModule } from '@angular/http';
import { AngularFireAuth } from '@angular/fire/auth';

import { CursoService } from './services/curso.service';
import { ChatService } from './services/chat.service';
import { CargaImagenService } from './services/carga-imagen.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CursosComponent } from './components/cursos/cursos.component';
import { AddCursoComponent } from './components/add-curso/add-curso.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AddPhotoComponent } from './components/gallery/add-photo.component';
import { MascotasComponent } from './components/mascotas/mascotas.component';
import { AddMascotaComponent } from './components/add-mascota/add-mascota.component';
import { ChatComponent } from './components/chat/chat.component';
import { LoginComponent } from './components/login/login.component';
import { ComidasComponent } from './components/comidas/comidas.component';
import { SidenavComponent } from './components/comidas/sidenav.component';
import { AddComidaComponent } from './components/add-comida/add-comida.component';
import { ListaComidaComponent } from './components/comidas/lista-comida.component';
import { NgDropFilesDirective } from './directives/ng-drop-files.directive';
import { PruebaDirectivaComponent } from './components/prueba-directiva/prueba-directiva.component';
import { HighlightDirective } from './directives/highligth.directive';
import { BuscarComponent } from './components/buscar/buscar.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CursosComponent,
    AddCursoComponent,
    GalleryComponent,
    AddPhotoComponent,
    MascotasComponent,
    AddMascotaComponent,
    ChatComponent,
    LoginComponent,
    ComidasComponent,
    SidenavComponent,
    AddComidaComponent,
    ListaComidaComponent,
    PruebaDirectivaComponent,
    NgDropFilesDirective,
    HighlightDirective,
    BuscarComponent 
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'AngularCrudCursos'),
    AngularFirestoreModule,
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTING,
    HttpModule,
    NgxPaginationModule
  ],
  providers: [CursoService, ChatService, AngularFireAuth, CargaImagenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
