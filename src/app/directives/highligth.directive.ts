import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { ImagenInterface } from '../models/imagen-interface';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  constructor(private el: ElementRef) {}
  @Input() defaultColor: string;
  @Input('appHighlight') highlightColor: string;

  @HostListener('mouseenter') onMouseEnter() {
     this.highlight(this.highlightColor || this.defaultColor || 'red');
  	//console.log(this.highlight);
  }
 
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
 

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}