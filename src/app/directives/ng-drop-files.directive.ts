import { Directive, EventEmitter, ElementRef, HostListener, Input, Output} from '@angular/core';
import { FileItem } from '../class/file-item';

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {
 
 @Input() archivo: FileItem[] = [];
 @Output() mouseSobre: EventEmitter<boolean> = new EventEmitter()
 @Output() imagenPuesta: EventEmitter<boolean> = new EventEmitter()

  constructor(el: ElementRef) {
  	console.log(this.archivo);
  }


 @HostListener('dragover', ['$event']) public onDragEnter(event: any){ // cuando esta encima, emite true
 	this.mouseSobre.emit(true)
 	this._prevenirDetener(event);
 }

 @HostListener('dragleave', ['$event']) public onDragLeave(event: any){  // cuando se quita, emite false
 	this.mouseSobre.emit(false)
 }

 @HostListener('drop', ['$event']) public onDrop(event: any){  // cuando se quita, emite false
 	this.mouseSobre.emit(false)
// :v
 	const transferencia = this._getTransferencia(event);

 	if(!transferencia){
 		return;
 	}

 	this._extraerArchivo(transferencia.files);

 	this._prevenirDetener(event);
 	this.mouseSobre.emit(false)
 }

// compatibilidad nav
 private _getTransferencia(event: any){
 	return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
 }

 private _extraerArchivo(archivoLista: FileList){
 	for(const propiedad in Object.getOwnPropertyNames(archivoLista)){
 		const archivoTemporal = archivoLista[propiedad];
 		if(this._paraCargarImagen(archivoTemporal)){
 			const nuevoArchivo = new FileItem(archivoTemporal);
 			this.archivo.push(nuevoArchivo);
 		}
 	}
 	this.imagenPuesta.emit(true)
 }

 //validar
 private _paraCargarImagen(archivo: File): boolean{
 	if(this._tipoImagen(archivo.type)){
 		return true;
 	}
 	else{
 		return false;
 	}
 }

 private _prevenirDetener(event){
 	event.preventDefault();
 	event.stopPropagation();
 }

 private _tipoImagen(tipoArchivo: string): boolean{
 	return ( tipoArchivo === '' || tipoArchivo === undefined ) ? false : tipoArchivo.startsWith('image');
 }


}
