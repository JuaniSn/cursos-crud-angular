import { Routes, RouterModule } from '@angular/router';
import { CursosComponent } from "./components/cursos/cursos.component";
import { GalleryComponent } from "./components/gallery/gallery.component";
import { MascotasComponent } from './components/mascotas/mascotas.component';
import { AddMascotaComponent } from './components/add-mascota/add-mascota.component';
import { LoginComponent } from './components/login/login.component';
import { ChatComponent } from './components/chat/chat.component';
import { ComidasComponent } from './components/comidas/comidas.component';
import { AddComidaComponent } from './components/add-comida/add-comida.component';
import { ListaComidaComponent } from './components/comidas/lista-comida.component';
import { PruebaDirectivaComponent } from './components/prueba-directiva/prueba-directiva.component';

const app_routes: Routes = [
	
	{ path: 'cursos', component: CursosComponent },
	{ path: 'gallery', component: GalleryComponent },
	{ path: 'mascotas', component: MascotasComponent },
	{ path: 'mascota/:id', component: AddMascotaComponent },
	{ path: 'chat', component: LoginComponent },
	{ path: 'prueba', component: PruebaDirectivaComponent },
	{ 
		path: 'comidas', 
		component: ComidasComponent,
		children: [
		  { path: 'lista', component: ListaComidaComponent },
		  { path: 'agregar', component: AddComidaComponent },
		  { path: 'editar/:id', component: AddComidaComponent },
		  { path: '**', pathMatch: 'full', redirectTo: 'lista' }
		] 
	},
	{ path: '**', pathMatch: 'full', redirectTo: 'cursos' }
	
];


export const APP_ROUTING = RouterModule.forRoot(app_routes);