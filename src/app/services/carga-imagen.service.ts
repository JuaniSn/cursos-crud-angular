import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';

import { FileItem } from '../class/file-item';

@Injectable({
  providedIn: 'root'
})
export class CargaImagenService {
  
  constructor(private storage: AngularFireStorage) { }

  
  uploadImagen( file: FileItem){
    const archivo = file.archivo
    const filePath = '/comidas/pic-' + Math.floor(Math.random() * 1000000);
      const task = this.storage.upload(filePath, archivo);
      task.percentageChanges().subscribe( value => {
        file.progreso = value;
      });
      console.log("PAsa de largo", file.progreso);
      task.then(() => {
           const ref = this.storage.ref(filePath);
           console.log('ya entre al then')
           const downloadURL = ref.getDownloadURL().subscribe(url => { 
           file.url = url 
           console.log('ya tas suscrito y esta es el por', file.progreso);
           console.log(file.url);
         });
    });

      console.log("Llega incluso aqui")
  }
}
