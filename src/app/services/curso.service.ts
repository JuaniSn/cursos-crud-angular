import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { CursoInterface } from '../models/curso-interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  cursosCollection: AngularFirestoreCollection<CursoInterface>;
  cursos: Observable<CursoInterface[]>;
  cursoDoc: AngularFirestoreDocument<CursoInterface>;

  constructor(private cualquierVerga: AngularFirestore) { 
  	this.cursosCollection = cualquierVerga.collection<CursoInterface>('cursos');    
  }


  getCursos(){
  	return this.cursos = this.cursosCollection.snapshotChanges().pipe(map(action => action.map(a => {
        let data = a.payload.doc.data() as CursoInterface;
        let id = a.payload.doc.id;
        return {id, data};
      }))
      );  
  }

  agregarCurso(curso: CursoInterface){
    console.log('Curso agregado: ', curso);
    this.cursosCollection.add(curso);
  }

  borrarCurso(curso: CursoInterface){
    console.log('Curso borrado :v', curso);
    this.cursoDoc = this.cualquierVerga.doc(`cursos/${curso.id}`);
    this.cursoDoc.delete();

  }


  actualizarCurso(curso: any){
    console.log('Curso actualizado :v', curso);
    this.cursoDoc = this.cualquierVerga.doc(`cursos/${curso.id}`);
    this.cursoDoc.update(curso.data);
  }




}
