import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { GalleryInterface } from '../models/gallery-interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

   imagenCollection: AngularFirestoreCollection<GalleryInterface>;
   addImagen: AngularFirestoreCollection;
   imagen: Observable<GalleryInterface[]>;
   imagenDoc: AngularFirestoreDocument<GalleryInterface>;
  // prueba: [];
  constructor(private cualquierVerga: AngularFirestore) { 
    this.imagen = cualquierVerga.collection('cursos').valueChanges();
  	 this.imagenCollection = cualquierVerga.collection<GalleryInterface>('imagenes');
     this.imagen = this.imagenCollection.snapshotChanges().pipe(
       map(action => action.map(a => {
         const data = a.payload.doc.data();
         const id = a.payload.doc.id;
         return {data, id};
       }))
      )

  }
  a(){
    return this.cualquierVerga.collection('imagenes').snapshotChanges();
  }


  getImagen(){
  	return this.imagen;
  }

  agregarImagen(imagen){
    return this.cualquierVerga.collection('imagenes').add(imagen);
  }


}
