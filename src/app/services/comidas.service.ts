import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';

import { ComidaInterface } from '../models/comida-interface';
import { FileItem } from '../class/file-item';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComidasService {
  
  comidaCollection: AngularFirestoreCollection<ComidaInterface>;
  comida: Observable<ComidaInterface[]>;
  comidaDocument: AngularFirestoreDocument<ComidaInterface[]>;

  constructor(private storage: AngularFireStorage, private db: AngularFirestore){
  	this.comidaCollection = db.collection<ComidaInterface>('comidas');
  }

  subirComida(comida: ComidaInterface){
	return this.comidaCollection.add(comida);
  }

  getComidas(){
  	return this.comidaCollection.snapshotChanges().pipe(map(action => action.map(upa => {
  		let data = upa.payload.doc.data() as ComidaInterface;
        let id = upa.payload.doc.id;
        return {id, data};
  	})));
  }

  dropComida(id: string){
     return this.comidaCollection.doc(id).delete();
  }

  obtenerComida(texto:string): any[]{
    let comida = this.getComidas();
    let comidaEncontrada: any[] = [];

    texto = texto.toLowerCase();

    comida.subscribe(data => {
        for(let buscar of data){
          let titulo = buscar.data.titulo.toLowerCase();
          if(titulo.indexOf(texto) >= 0){
            comidaEncontrada.push(buscar);
          }
        }
    })
       return comidaEncontrada;
  }

  getComidaEditar(id: string){
     return this.comidaCollection.doc(id).snapshotChanges();
  }

}