import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { MensajeInterface } from '../models/mensaje-interface';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  
  chatCollection: AngularFirestoreCollection<MensajeInterface>;
  chats: MensajeInterface[];

  usuario:any = {};

  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth){
    this.afAuth.authState.subscribe(user => {
      console.log('Estado: ', user);

      if(!user){
        return;
      }

      this.usuario.nombre = user.displayName;
      this.usuario.uid = user.uid;
    });

  }

  login(method:string) {
    if(method == 'google'){
      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    }
    else{
      this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider());
    }
  }
  
  logout() {
    this.usuario = {};
    this.afAuth.auth.signOut();
  }


  cargarMensajes(){
  	this.chatCollection = this.afs.collection<MensajeInterface>('chat', query => query.orderBy('fecha', 'desc').limit(5));

    return this.chatCollection.valueChanges().pipe(map((mensajes: MensajeInterface[]) =>{
       //console.log(mensajes);
       this.chats = [];

       for(let i of mensajes){
         this.chats.unshift(i);
       }

       console.log(this.chats);
       return this.chats;
   }))

  }

  agregarMensaje(texto: string){
    let mensaje: MensajeInterface = {
      nombre: this.usuario.nombre,
      fecha: new Date().getTime(),
      mensaje: texto,
      uid: this.usuario.uid
    }

    return this.chatCollection.add(mensaje);
  }


}
