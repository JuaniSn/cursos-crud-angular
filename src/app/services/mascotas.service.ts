import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Mascota } from '../models/mascota-interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MascotasService {

   mascotaCollection: AngularFirestoreCollection<Mascota>;
   mascotas: Observable<Mascota[]>;
   mascotaDocument: AngularFirestoreDocument<Mascota[]>;

   respuesta:any;

   constructor(private peticion: AngularFirestore) { 
     this.mascotaCollection = peticion.collection<Mascota>('mascotas');
   }


  nuevaMascota(mascota:Mascota){
      return this.mascotaCollection.add(mascota);
  }

  actualizarMascota(mascota: Mascota, documentId: string) {
      return this.mascotaCollection.doc(documentId).set(mascota);
  }

  obtenerMascota(documentId: string) {
     return this.mascotaCollection.doc(documentId).snapshotChanges();
  }

  obtenerMascotas(){
    return this.mascotaCollection.snapshotChanges().pipe(map( action => action.map( b => {
        let data = b.payload.doc.data() as Mascota;
        let id = b.payload.doc.id;
        return {id, data};
    })));
  }

  eliminarMascota(documentId: string){
    return this.mascotaCollection.doc(documentId).delete();
  }
  




}
