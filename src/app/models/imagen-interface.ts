export interface ImagenInterface{
	lastModified?: number;
  	lastModifiedDate?: string;
	name?: string;
	size?: number;
​	type?: string;
	webkitRelativePath?: string;
}