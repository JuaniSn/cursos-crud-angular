export interface CursoInterface {
	
	id?: string;
	name?: string;
	facilitator?: string;
	price?: string;
	language?: string;
	date?: any;
	technology?: string;
	description?: string;

}
