export interface Mascota{
	nombre: string;
	raza: string;
	edad: string;
	key$?: string;
}