export interface ComidaInterface{
  	titulo?: string;
  	descripcion?: string;
  	ingredientes?: string;
  	preparacion?: string;
  	persona?: string;
  	fecha?: number;
  	url?: string;
}