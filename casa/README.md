# Cursos

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## CMD used

npm i --save firebase angularfire2

Carpeta Backend:

<?php  

header("Access-Control-Allow-Origin: *");

//var_dump($_FILES);

$publicKey = '50d4e4b37cb2b2ea627b';
$rutaTemporal = $_FILES['file']['tmp_name'];
$nombreActual = $_FILES['file']['name'];

$rutaActual = dirname(__FILE__).$nombreActual;


$res = move_uploaded_file($rutaTemporal, $rutaActual);

$urlSubir = 'https://upload.uploadcare.com/base/';
$ch = curl_init();

 $post = array( 
 	'UPLOADCARE_PUB_KEY' => $publicKey,
 	'UPLOADCARE_STORE' => 1,
 	'file'		=> curl_file_create($rutaActual)
 );

 curl_setopt($ch, CURLOPT_URL, $urlSubir);
 curl_setopt($ch, CURLOPT_POST, 1);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

 $response = curl_exec($ch);


echo $response;